package com.restapp.model;

import org.springframework.stereotype.Repository;
@Repository
public class SummaryResultLine {
	private String currency;
	private String type;
	private double price;
	private double commission;
	private double toChargeValue;
	private double settlementValue;
	
	public SummaryResultLine(String currency, String type, double price, double commission, double toChargeValue, double settlementValue ){
		this.type = type; 
		this.price = price;
		this.commission = commission;
		this.currency = currency;
		this.toChargeValue = toChargeValue;
		this.settlementValue = settlementValue;
	
	}
	public SummaryResultLine(String currency, String type, double price, double commission ){
		this.type = type; 
		this.price = price;
		this.commission = commission;
		this.currency = currency;
	}
	
	public SummaryResultLine(){	
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getCommission() {
		return commission;
	}
	public void setCommission(double commission) {
		this.commission = commission;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public double getToChargeValue() {
		return toChargeValue;
	}
	public void setToChargeValue(double toChargeValue) {
		this.toChargeValue = toChargeValue;
	}

	public double getSettlementValue() {
		return settlementValue;
	}
	public void settlementValue(double settlementValue) {
		this.settlementValue = settlementValue;
	}
	
	public String toString(){
		return "{\"currency\":\"" + this.currency + "\",\"type\":\"" + this.type + "\",\"price\":\"" + this.price + 
				"\",\"commission\":\"" + this.commission + "\",\"toChargeValue\":" + this.toChargeValue +
				",\"settlementValue\":" + this.settlementValue + "}";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(commission);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((currency == null) ? 0 : currency.hashCode());
		temp = Double.doubleToLongBits(price);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(settlementValue);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(toChargeValue);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SummaryResultLine other = (SummaryResultLine) obj;
		if (Double.doubleToLongBits(commission) != Double.doubleToLongBits(other.commission))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (Double.doubleToLongBits(price) != Double.doubleToLongBits(other.price))
			return false;
		if (Double.doubleToLongBits(settlementValue) != Double.doubleToLongBits(other.settlementValue))
			return false;
		if (Double.doubleToLongBits(toChargeValue) != Double.doubleToLongBits(other.toChargeValue))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	} 
	
	public double countSettlementValue(double price, double comission, double toChargeValue){
		return price - comission - toChargeValue;
	}
	
}
