package com.restapp.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Repository;

import com.restapp.model.Transaction;

@Repository
public class TransactionDAOImpl implements ITransactionDAO{
	
	public TransactionDAOImpl(){	
	}
	
	@Override
	public Resource getPathToTransactionsResource(String fileName){
		//get relative path to file with data
		final DefaultResourceLoader loader = new DefaultResourceLoader();
		Resource resource = null;
		try{
			resource = loader.getResource("classpath:" + fileName);
		}catch(Exception e){
			e.printStackTrace();
		}
		return resource;
		
	}
	
	@Override
	public ArrayList<Transaction> getAllTransactions() {
		ArrayList<Transaction> transactions = new ArrayList<Transaction>(); //collection for transactions data
		
		File file;
		BufferedReader br = null;
		try {
			file = this.getPathToTransactionsResource("transactions.csv").getFile();
			br = new BufferedReader(new FileReader(file));
		}
		catch(FileNotFoundException fileNotFoudExeption){
			fileNotFoudExeption.printStackTrace();
		}
		catch (IOException fileException) {
			fileException.printStackTrace();
		}  
	
		String line; //single line from data file
		try{
			while ((line = br.readLine()) != null) {

			    String[] entries = line.split(";");

				Transaction transaction = new Transaction(Integer.parseInt(entries[0]),(String) entries[1].replaceAll("[^\\w]", ""), 
						Double.parseDouble(entries[2]), Double.parseDouble(entries[3]),(String) entries[4].replaceAll("[^\\w]", ""), 
						Boolean.parseBoolean(entries[5]));

			    transactions.add(transaction);
			}
		}catch(IOException e){
			e.printStackTrace();
		}finally{
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return transactions;
	}
}
