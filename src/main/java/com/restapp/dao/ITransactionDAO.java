package com.restapp.dao;

import org.springframework.core.io.Resource;

import java.util.ArrayList;

import com.restapp.model.Transaction;

public interface ITransactionDAO {
	public ArrayList<Transaction> getAllTransactions();
	public Resource getPathToTransactionsResource(String fileName);
}
