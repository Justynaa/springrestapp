package com.restapp.controller;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.restapp.model.SummaryResultLine;
import com.restapp.service.IReportingService;


@RestController
@RequestMapping(value = "/rest")
public class AppRestController {
	private static final Logger logger = Logger.getLogger(AppRestController.class);
	
	private IReportingService rs;
	
	@Autowired // Using constructor Injection
	public AppRestController(IReportingService rs){
		this.rs = rs;
	}

	@RequestMapping(value = "/{currency}/{type}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<SummaryResultLine> findSpecifiedTransactionSummary (@PathVariable(value = "currency") String currency,
			@PathVariable(value = "type") String type) throws NumberFormatException, IOException {
		SummaryResultLine result = rs.getSpecifiedTransactionSummary(currency, type);
        if(result == null){
            return new ResponseEntity<SummaryResultLine>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<SummaryResultLine>(result, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/summary", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ArrayList<SummaryResultLine>> findAllTransactionsSummarySorted() {
		ArrayList<SummaryResultLine> results = rs.getAllTransactionsSummarySortedByCurrency();
		if(results.isEmpty()){
			return new ResponseEntity<ArrayList<SummaryResultLine>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<ArrayList<SummaryResultLine>>(results, HttpStatus.OK);
	}
	
}
