package com.restapp.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.restapp.dao.ITransactionDAO;
import com.restapp.model.SummaryResultLine;
import com.restapp.model.Transaction;

@Service
public class ReportingServiceImpl implements IReportingService {
	@Autowired
	private ITransactionDAO transactionDAO;
		
	@Override
	public ArrayList<SummaryResultLine> getAllTransactionsSummary(){
		ArrayList<Transaction> transactions = null; //get list of transactiins from DAO
		try {
			transactions = (ArrayList<Transaction>) transactionDAO.getAllTransactions();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		ArrayList<SummaryResultLine> summarizedResults = summarizeTransactions(transactions);
		return summarizedResults;	
		
	}
	
	@Override
	public SummaryResultLine getSpecifiedTransactionSummary(String currency, String type){
		ArrayList<Transaction> selectedTransactions = null;
		try {
			selectedTransactions = (ArrayList<Transaction>) transactionDAO.getAllTransactions();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		selectedTransactions = (ArrayList<Transaction>) selectedTransactions.stream()
				.filter(r -> (r.getCurrency().equals(currency.trim().toUpperCase()) && r.getType().equals(type.trim().toLowerCase()))).collect(Collectors.toList());
		if(selectedTransactions.isEmpty()) return null;
		else{
			ArrayList<SummaryResultLine> summarizedResult = summarizeTransactions(selectedTransactions);
			return summarizedResult.get(0);
		}	
	}
	
	@Override
	public ArrayList<SummaryResultLine> getAllTransactionsSummarySortedByCurrency(){
		return this.sortResultsByCurrency(this.getAllTransactionsSummary());
	}
	
	@Override
	public ArrayList<SummaryResultLine> sortResultsByCurrency(ArrayList<SummaryResultLine> results){
		Collections.sort(results, (SummaryResultLine o1, SummaryResultLine o2) ->{
	        return o1.getCurrency().compareToIgnoreCase(o2.getCurrency());
		});
		return results;
	}
	
	//In summarizeTransactions() we iterate over received list of transactions 
	// and we put the summary in HashMap where key is String created from currency name and type of transaction.
	//Final key is created by currency name in upper cases and type of transaction in lower cases.
	@Override
	public ArrayList<SummaryResultLine> summarizeTransactions(ArrayList<Transaction> transactions){
		HashMap<String, SummaryResultLine> resultsMap = new HashMap<String, SummaryResultLine>();
		
		for(Transaction t : transactions){
			double tempPrice = 0;
			double tempCommission = 0;
			double tempToCharge = 0;
			double tempSettlementValue = 0;
			
		    if(!resultsMap.containsKey(t.getCurrency().concat(t.getType()))) { //if there is no key in the map
		       tempPrice = t.getPrice(); 
		       tempCommission = t.getCommission();
		       if(t.isPaid() == false){
		    	   tempToCharge = t.getPrice();
		       }
		       tempSettlementValue = tempPrice - tempCommission - tempToCharge;
		    } else {    //if key is in the map we get previous values from map, add current values to it and save new value to map
		    	SummaryResultLine tempSrl = resultsMap.get(t.getCurrency().concat(t.getType()));
		        tempPrice = t.getPrice() + tempSrl.getPrice();
		        tempCommission = t.getCommission() + tempSrl.getCommission();
		        tempToCharge = tempSrl.getToChargeValue();
		        if(t.isPaid() == false) tempToCharge += t.getPrice();
		        tempSettlementValue = tempPrice - tempCommission - tempToCharge;
		    }
			SummaryResultLine srl = new SummaryResultLine(t.getCurrency(), t.getType(), tempPrice, tempCommission, tempToCharge, tempSettlementValue);
			
			resultsMap.put(t.getCurrency().concat(t.getType()), srl);
		}
		Collection<SummaryResultLine> summaryValuesCollection = resultsMap.values();	
		ArrayList<SummaryResultLine> listOfValues = new ArrayList<SummaryResultLine>(summaryValuesCollection);
		return listOfValues;
	}

}

