package com.restapp.service;

import java.util.ArrayList;

import com.restapp.model.SummaryResultLine;
import com.restapp.model.Transaction;


public interface IReportingService {	
	public ArrayList<SummaryResultLine> getAllTransactionsSummary();
	public SummaryResultLine getSpecifiedTransactionSummary(String currency, String type);
	public ArrayList<SummaryResultLine> getAllTransactionsSummarySortedByCurrency();
	public ArrayList<SummaryResultLine> sortResultsByCurrency(ArrayList<SummaryResultLine> results);
	public ArrayList<SummaryResultLine> summarizeTransactions(ArrayList<Transaction> transactions);
}
