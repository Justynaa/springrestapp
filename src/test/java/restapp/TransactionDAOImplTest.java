package restapp;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.restapp.dao.ITransactionDAO;
import com.restapp.dao.TransactionDAOImpl;
import com.restapp.model.Transaction;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class TransactionDAOImplTest {
    @Autowired
    private ITransactionDAO transactionDAO;
    
	@Test
	public void getAllTransactions_TransactionsFound_ShouldReturnListOfTransactions() {
		ArrayList<Transaction> transactions = transactionDAO.getAllTransactions();

		assertEquals(6, transactions.size());
		
		assertEquals(1, transactions.get(0).getId());
		assertEquals(20.0, transactions.get(0).getPrice(), 0.0f);
		assertEquals("trip", transactions.get(0).getType());
		assertEquals(5.0, transactions.get(0).getCommission(), 0.0f);
		assertEquals("EUR", transactions.get(0).getCurrency());
		
		assertEquals(4, transactions.get(3).getId());
		assertEquals(100.0, transactions.get(3).getPrice(), 0.0f);
		assertEquals("transfer", transactions.get(3).getType());
		assertEquals(0.0, transactions.get(3).getCommission(), 0.0f);
		assertEquals("PLN", transactions.get(3).getCurrency());
	}
	
	@Test
	public void getPathToTransactionsResource_ResourceExists_ShouldReturnResource(){
		Resource resource = transactionDAO.getPathToTransactionsResource("transactions.csv");
		assertEquals(true, resource.exists());
		assertEquals("transactions.csv", resource.getFilename());
	}

	@Test
	public void getPathToTransactionsResource_ResourceNotExists_ShouldThrowException() {
		Resource resource;
		resource = transactionDAO.getPathToTransactionsResource("wrongtransactions.csv");
	    	
		assertEquals(false, resource.exists());
	}

	
	@Configuration
	static class TransactionDAOTestContextConfiguration {
		@Bean
	    public ITransactionDAO transactionRepository() {
	        return new TransactionDAOImpl();
	    }
	}
}
