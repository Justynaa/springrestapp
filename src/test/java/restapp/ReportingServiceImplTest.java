package restapp;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.restapp.dao.ITransactionDAO;
import com.restapp.model.SummaryResultLine;
import com.restapp.model.Transaction;
import com.restapp.service.IReportingService;
import com.restapp.service.ReportingServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class ReportingServiceImplTest {
    @Autowired
    private IReportingService reportingService;
    @Autowired
    private ITransactionDAO transactionDAO;
	
    @Before
    public void setup() {
    	ArrayList<Transaction> transactions = new ArrayList<Transaction>();
		transactions.add(new Transaction(1, "trip", 20.0, 5.0, "EUR", true));
		transactions.add(new Transaction(2, "ticket", 10.0, 2.0, "EUR", true));
		transactions.add(new Transaction(3, "trip", 80.0, 20.0, "PLN", false));
		transactions.add(new Transaction(4, "transfer", 100.0, 0.0, "PLN", true));
		transactions.add(new Transaction(5, "trip", 50.0, 18.0, "EUR", true));
		transactions.add(new Transaction(6, "trip", 120.0, 5.0, "PLN", true));
        Mockito.when(transactionDAO.getAllTransactions()).thenReturn(transactions);
        
    }
    //test of specified lines for EUR-trip and PLN trip
	@Test
	public void getSpecifiedTransactionSummary_Found_ShouldReturnSummatyLineWithValues(){
		SummaryResultLine srl = reportingService.getSpecifiedTransactionSummary("EUR", "trip");
		assertEquals("EUR", srl.getCurrency());
		assertEquals("trip", srl.getType());
		assertEquals(70.0, srl.getPrice(), 0.0f);
		assertEquals(0.00, srl.getToChargeValue(), 0.0f);
		assertEquals(47.00, srl.getSettlementValue(), 0.0f);
		
		SummaryResultLine srl2 = reportingService.getSpecifiedTransactionSummary("PLN", "trip");
		assertEquals("PLN", srl2.getCurrency());
		assertEquals("trip", srl2.getType());
		assertEquals(200.0, srl2.getPrice(), 0.0f);
		assertEquals(80.00, srl2.getToChargeValue(), 0.0f);
		assertEquals(95.00, srl2.getSettlementValue(), 0.0f);
	}
	
	@Test
	public void getAllTransactionsSumary_Found_ShouldReturnSummarizedData(){
		ArrayList<SummaryResultLine> summary = reportingService.getAllTransactionsSummary();
		System.out.println(summary.toString());
		assertEquals(4, summary.size());
		assertEquals("EUR", summary.get(2).getCurrency());
		assertEquals("trip", summary.get(2).getType());
		assertEquals(70.0, summary.get(2).getPrice(), 0.0f);
		assertEquals(23.0, summary.get(2).getCommission(), 0.0f);
		assertEquals(0.00, summary.get(2).getToChargeValue(), 0.0f);
		assertEquals(47.00, summary.get(2).getSettlementValue(), 0.0f);
	}
	
	@Test
	public void getAllTransactionsSumarySortedByCurrency_Found_ShouldReturnSummarizedDataSortedByCurrency(){
		ArrayList<SummaryResultLine> summary = reportingService.getAllTransactionsSummarySortedByCurrency();
		System.out.println(summary.toString());
		assertEquals(4, summary.size());
		assertEquals("PLN", summary.get(2).getCurrency());
		assertEquals("trip", summary.get(2).getType());
		assertEquals(200.0, summary.get(2).getPrice(), 0.0f);
		assertEquals(25.0, summary.get(2).getCommission(), 0.0f);
		assertEquals(80.00, summary.get(2).getToChargeValue(), 0.0f);
		assertEquals(95.00, summary.get(2).getSettlementValue(), 0.0f);
	}
	
	@Configuration
	static class ReportingServiceTestContextConfiguration {
		@Bean
	    public IReportingService accountService() {
	       return new ReportingServiceImpl();
		}
		@Bean
	    public ITransactionDAO transactionRepository() {
	        return Mockito.mock(ITransactionDAO.class);
	    }
	}

}
