package restapp;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.restapp.controller.AppRestController;
import com.restapp.model.SummaryResultLine;
import com.restapp.service.IReportingService;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration
public class AppRestControllerTest {
	private MockMvc mockMvc;
    
	@Mock
    private IReportingService reportingServiceMock;

    @Before
    public void setup() {
    	final AppRestController appRestController = new AppRestController(reportingServiceMock);
        mockMvc = MockMvcBuilders.standaloneSetup(appRestController)
                .build();

       	ArrayList<SummaryResultLine> results = new ArrayList<SummaryResultLine>();
    	results.add(new SummaryResultLine("EUR","trip", 70.0, 23.0, 0.0, 47.0));
    	results.add(new SummaryResultLine("PLN","trip", 200.0, 25.0, 80.0, 95.0));
        
        Mockito.when(reportingServiceMock.getSpecifiedTransactionSummary("EUR", "trip")).thenReturn(results.get(0));
        
    }
	
	@Test
	public void findAllTransactionsSummarySortedByCurrency_Found_shouldReturnSortedArrayOfSummaryLines() throws Exception {
       	ArrayList<SummaryResultLine> results = new ArrayList<SummaryResultLine>();
    	results.add(new SummaryResultLine("EUR","trip", 70.0, 23.0, 0.0, 47.0));
    	results.add(new SummaryResultLine("PLN","trip", 200.0, 25.0, 80.0, 95.0));
		Mockito.when(reportingServiceMock.getAllTransactionsSummarySortedByCurrency()).thenReturn(results);
		
		mockMvc.perform(get("/rest/summary"))
		.andExpect(status().isOk())
		.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
        .andExpect(jsonPath("$", hasSize(2)))
        .andExpect(jsonPath("$[0].currency", is("EUR")))
        .andExpect(jsonPath("$[0].type", is("trip")))
        .andExpect(jsonPath("$[0].price", is(70.0)))
        .andExpect(jsonPath("$[0].commission", is(23.0)))
        .andExpect(jsonPath("$[0].toChargeValue", is(0.0)))
        .andExpect(jsonPath("$[0].settlementValue", is(47.0)))
        .andExpect(jsonPath("$[1].currency", is("PLN")))
        .andExpect(jsonPath("$[1].type", is("trip")))
        .andExpect(jsonPath("$[1].price", is(200.0)))
        .andExpect(jsonPath("$[1].commission", is(25.0)))
        .andExpect(jsonPath("$[1].toChargeValue", is(80.0)))
        .andExpect(jsonPath("$[1].settlementValue", is(95.0)));
      
        verify(reportingServiceMock, times(1)).getAllTransactionsSummarySortedByCurrency();
        verifyNoMoreInteractions(reportingServiceMock);
	}
	
	@Test
	public void findAllTransactionsSummarySortedByCurrency_NoContent_shouldReturnNoContent() throws Exception {
		ArrayList<SummaryResultLine> results = new ArrayList<SummaryResultLine>();
		Mockito.when(reportingServiceMock.getAllTransactionsSummarySortedByCurrency()).thenReturn(results);
		
		mockMvc.perform(get("/rest/summary"))
		.andExpect(status().isNoContent())
        .andExpect(jsonPath("$").doesNotExist());
        verify(reportingServiceMock, times(1)).getAllTransactionsSummarySortedByCurrency();
        verifyNoMoreInteractions(reportingServiceMock);
	}
	
	@Test  
	public void findSpecifiedTransactionSummary_Found_ShouldReturnFoundSummaryLine() throws Exception {
		mockMvc.perform(get("/rest/{currency}/{type}", "EUR", "trip"))
	    	.andExpect(status().isOk())
	        .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
	        .andExpect(jsonPath("$.currency", is("EUR")))
	        .andExpect(jsonPath("$.type", is("trip")))
	        .andExpect(jsonPath("$.price", is(70.0)))
	        .andExpect(jsonPath("$.commission", is(23.0)))
	        .andExpect(jsonPath("$.toChargeValue", is(0.0)))
	        .andExpect(jsonPath("$.settlementValue", is(47.0)));
	 
	        verify(reportingServiceMock, times(1)).getSpecifiedTransactionSummary("EUR", "trip");
	        verifyNoMoreInteractions(reportingServiceMock);
	}	
	
	@Test  
	public void findSpecifiedTransactionSummary_NotFound_ShouldReturnNotFound() throws Exception {
		mockMvc.perform(get("/rest/{currency}/{type}", "CAD", "trip"))
	    	.andExpect(status().isNotFound())
	        .andExpect(jsonPath("$").doesNotExist());
	        verify(reportingServiceMock, times(1)).getSpecifiedTransactionSummary("CAD", "trip");
	        verifyNoMoreInteractions(reportingServiceMock);
	        
	 }	
	
	@Configuration
	static class AppRestControllerTestContextConfiguration {
		@Bean
	    public IReportingService accountService() {
	       return Mockito.mock(IReportingService.class);
		}
	}


}
