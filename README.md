# README #

Simple Spring REST service that enables fetching data from csv file (src/main/resources/transactions.csv) and provides summary of received data by rest controller.

In order to get data summarized by curency (EUR or PLN) and type (trip, ticket, transfer) use below address: http://localhost:8080/TransactionSummary/rest/currency/type

* Java 8
* Spring 4.3.5.RELEASE
* Tomcat 9.0
* Maven